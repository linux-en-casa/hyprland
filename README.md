# Hyprland en Arch Linux

## Actualización - Mayo 24 de 2024
Pruebo script de configuración y corrijo archivo de configuración de hyprland.
Cambiaron algunos detalles en algunas líneas.
Probado en Compa CQ45 - OK

---

Archivos de configuración de Hyprland en Arch Linux.
* Barra de Waybar.
* Wofi, para lanzar aplicaciones.
* EWW, para mostrar los atajos de teclado.
* Los scripts: WofiPower y LockScreen.

Dentro de la subcarpeta eww hay otros scripts para las teclas de volumen.

## Instalación

La instalación y configuración la explico (paso a paso) en:
[YouTube, Linux en Casa](https://youtu.be/YDHL_ltKubs) .

También hice un [Directo en YouTube](https://youtube.com/live/z3_BYTauJ9M) utilizando el script de instalación (hyprland-install.sh) .

Se resume en 5 pasos:
1. Instalar Arch Linux con el script archinstall, en Perfil y en Type seleccionar Hyprland.
2. Post - Instalación, actualizar e instalar git.

    `sudo pacman -Syu git`

3. Descargar script de instalación:

    `git clone https://gitlab.com/linux-en-casa/hyprland.git`

4. Ejecutar script:

    `cd hyprland`

    `./hyprland-install`

5. Reiniciar:

    `systemctl reboot`


## Notas
1. eww se instala desde repos de github con Rust.  Archivo en: /usr/local/bin/eww .

2. para los atajos de subir y bajar volumen uso algunos scripts de eww que encontré en el repositorio de  [GitHub de Linkfrg](https://github.com/linkfrg) .

Aunque no copié todos los archivos, solo los necesarios para que funcionen las teclas del volumen.

3. También se pueden instalar otros paquetes como:

    `sudo pacman -Syu pavucontrol less vim libnotify`

4. Esta configuración la tomé de varios sitios, entre ellos:
   * Distros de Arco Linux y Garuda Linux.
   * [GitHub de JaKooLit](https://github.com/JaKooLit) .
   * [GitHub de Linkfrg](https://github.com/linkfrg) .


**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.



