#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    exit 1
fi


echo "Configurar Hyprland - Waybar wofi y eww"
echo "En Arch Linux"
echo "Partiendo de la instalación con archinstall"
echo "Instalando hyprland como el Desktop y sddm como DM"
echo "Advertencia -----  Advertencia"
echo "Probada en VM Virt-Manager"

sudo pacman -Syu
echo "Instalar paquetes necesarios desde repositorios oficiales"
sudo pacman -S  --noconfirm waybar firefox adobe-source-code-pro-fonts noto-fonts-emoji otf-font-awesome ttf-droid ttf-fantasque-sans-mono ttf-jetbrains-mono-nerd git neofetch base-devel xdg-user-dirs swaylock pamixer

echo "Instalar y configurar yay"
cd /tmp/
git clone https://aur.archlinux.org/yay.git
cd /tmp/yay
makepkg -si


# configurar carpetas de usuario
xdg-user-dirs-update

# clonar repositorio de gitlab de hyprland
cd /tmp
git clone https://gitlab.com/linux-en-casa/hyprland.git
cd /tmp/hyprland
cp -r eww/ hypr/ kitty/ scripts/ waybar/ wofi/ ~/.config/

# instalar rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# instalado rust pero tal vez no funcione la instalación de eww
# porque debo reiniciar la shell para que rust funcione
# si doy este comando funciona:
source "$HOME/.cargo/env"

echo "instalar eww desde repositorio de github"
echo "tal vez da error porque rust necesita reiniciar la shell"

cd /tmp
git clone https://github.com/elkowar/eww
cd /tmp/eww
cargo build --release --no-default-features --features=wayland
sudo cp /tmp/eww/target/release/eww /usr/local/bin


# Mensaje de Instalado

echo "Hyprland Configurado con waybar y wofi"
echo "eww Instalado"
echo "..."
echo "..."
echo "fin de instalación"
echo "Cerrar Sesión o Reiniciar"
